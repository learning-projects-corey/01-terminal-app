package corey;

import java.util.Scanner;

/**
 * Shrooms for 01-terminal-app
 * (public class main) - the class field identifies that this is a class
 * a class is the base of any projects
 */


public class Main {

    /*
    This is a field something that is equal to something or will reference that data type or object is a field
     */
    public static final String PROJECT_TITLE = "Shroom Doom";

    public static void main(String[] args) {
        System.out.println(PROJECT_TITLE);
        System.out.println("\nShroom Options:\n");
        System.out.println("1. Magic shrooms");
        System.out.println("2. psylocybe Shroom");
        System.out.println("3. Cap Shroom");

        System.out.println("\n\nSelect a second shroom: ");
        Scanner shroomScanner = new Scanner(System.in);
        int optionIn = shroomScanner.nextInt();
        System.out.println("You Selected a Shroom " + optionIn);

        if (optionIn == 1) {
            System.out.println("Good Evening");
        } else if (optionIn == 2) {
            System.out.println("Farewell Weary Traveler");
        } else if (optionIn == 3) {
            System.out.println("Backeth Offeth from the Shroometh");
        } else {
            System.out.println("Wrong");
        }
    }
}